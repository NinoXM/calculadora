import React, { useState } from 'react'

const Calculadora = () => {
  const [result, setResult] = useState("");

  const handleClick = (e) => {
    if (e.target.name === "%") {
      setResult(result.concat(e.target.name));
    } else {
      setResult(result.concat(e.target.name));

    }
  }

  const backspace = () => {
    setResult(result.slice(0, -1));
  }


  const clear = () => {
    setResult("");
  }

  const calculate = () => {
    try {
      if (result.includes("%")) {
        let newResult = result.replace("%", "/100");
        setResult(eval(newResult).toString());
      } else {
        setResult(eval(result).toString());
      }
    } catch (err) {
      setResult("Error")
    }
  }

  return (
    <div className="container">
      <div className='row'>
        <h1 ><span className='text-white'>Calcu</span><span className='text-danger'>ladora</span></h1>
        </div>

      <div className='card-body p-4 w-100 bg-dark'>

          <div className="row">
            <form>
              <input type="text" className='form-control bg-light text-dark '
                value={result} />
            </form>

          </div>

          <div className="row mt-2">
            <button className='btn btn-primary col-2' name="+" onClick={handleClick}>+</button>
            <button className='btn btn-primary col-2' name="-" onClick={handleClick}>-</button>
            <button className='btn btn-primary col-2' name="*" onClick={handleClick}>*</button>
            <button className='btn btn-primary col-2' name="/" onClick={handleClick}>/</button>
            <button className='btn btn-primary col-4' onClick={backspace} id="backspace">
              <i className="bi bi-arrow-left" ></i>
            </button>
          </div>

       
        <div className='row'>
          <button className='btn btn-secondary col-4' name="0" onClick={handleClick}>0</button>
          <button className='btn btn-secondary col-4 ' name="." onClick={handleClick}>.</button>
          <button className='btn btn-secondary col-4 ' name="%" onClick={handleClick}>%</button>
        </div>
        <div className='row'>
          <button className='btn btn-secondary col-4 ' name="7" onClick={handleClick}>7</button>
          <button className='btn btn-secondary col-4 ' name="8" onClick={handleClick}>8</button>
          <button className='btn btn-secondary col-4 ' name="9" onClick={handleClick}>9</button>
        </div>

        <div className='row'>
          <button className='btn btn-secondary col-4' name="4" onClick={handleClick}>4</button>
          <button className='btn btn-secondary col-4' name="5" onClick={handleClick}>5</button>
          <button className='btn btn-secondary col-4' name="6" onClick={handleClick}>6</button>
        </div>


        <div className='row'>
          <button className='btn btn-secondary col-4' name="1" onClick={handleClick}>1</button>
          <button className='btn btn-secondary col-4' name="2" onClick={handleClick}>2</button>
          <button className='btn btn-secondary col-4' name="3" onClick={handleClick}>3</button>
        </div>
        <div className='row '>
          <button className='btn btn-primary col-6 ' onClick={clear} id="clear">C</button>
          <button className='btn btn-primary col-6 ' onClick={calculate} id="result">=</button>
        </div>



      </div>

    </div>
  );
}

export default Calculadora
