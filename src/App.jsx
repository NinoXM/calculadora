import { useState } from 'react'
import './App.css'
import Calculadora from './Calculadora'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css"
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div className="bg-gray">
        <Calculadora></Calculadora>
      </div>
   
    </>
  )
}

export default App
